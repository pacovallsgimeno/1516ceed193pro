<?php

namespace app\controllers;

use Yii;
use app\models\Diarios;
use app\models\DiariosSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;


/**
 * DiariosController implements the CRUD actions for Diarios model.
 */
class DiariosController extends Controller
{
	
    
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Diarios models.
     * @return mixed
     */
    public function actionIndex()
    {
    	$userlog = Yii::$app->user->identity->id;
    	
    	$searchModel = new DiariosSearch(['userid'=> $userlog,]);
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        	'userlog'=> $userlog,	
        ]);
    }

    /**
     * Displays a single Diarios model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
    	
    	
        return $this->render('view', [
            'model' => $this->findModel($id),
        	
        ]);
    }

    /**
     * Creates a new Diarios model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
    	$userlog = Yii::$app->user->identity->id;
    	
    	$model = new Diarios();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->apunteid]);
        } else {
            return $this->render('create', [
                'model' => $model,
            	'userlog' => $userlog,	
            ]);
        }
    }

    /**
     * Updates an existing Diarios model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
    	$userlog = Yii::$app->user->identity->id;
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->apunteid]);
        } else {
            return $this->render('update', [
                'model' => $model,
            	'userlog' => $userlog,
            ]);
        }
    }

    /**
     * Deletes an existing Diarios model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }
    
  

    /**
     * Finds the Diarios model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Diarios the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Diarios::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
    
  
    
  
    
  
    
  
}
