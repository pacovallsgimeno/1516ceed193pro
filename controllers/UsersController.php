<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use app\models\User;
use app\models\Users;
use app\models\UsersSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * UsersController implements the CRUD actions for Users model.
 */
class UsersController extends Controller
{
   /*  /**
     * @inheritdoc
     */
 /*   public function behaviors()
   {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    } */
	
    // Control de Acceso usuarios en la aplicacion
    public function behaviors() {
    	return [
    			'access' => [
    					'class' => AccessControl::className (),
    					'only' => [
    							'logout',
    							'user',
    							'admin',
    							'delete',
    						  	'update',
    							'view',
    							'findmodel',
    							'index',
    							
    					],
    					'rules' => [
    							[
    									// El administrador tiene permisos sobre las siguientes acciones
    									'actions' => [
    											'delete',
    											'update',
    											'view',
    											'findModel',
    											'index',
    												
    
    									],
    									// Esta propiedad establece que tiene permisos
    									'allow' => true,
    									// Usuarios autenticados, el signo ? es para invitados
    									'roles' => [
    											'@',
    
    											''
    									],
    									// Este m�todo nos permite crear un filtro sobre la identidad del usuario
    									// y as� establecer si tiene permisos o no
    									'matchCallback' => function ($rule, $action) {
    									// Llamada al m�todo que comprueba si es un administrador
    									return User::isUserAdmin ( Yii::$app->user->identity->id );
    									}
    									],
    									[
    											// Los usuarios simples tienen permisos sobre las siguientes acciones
    											'actions' => [
    													'',
    													''
    											],
    											// Esta propiedad establece que tiene permisos
    											'allow' => true,
    											// Usuarios autenticados, el signo ? es para invitados
    											'roles' => [
    													'@'
    											],
    											// Este m�todo nos permite crear un filtro sobre la identidad del usuario
    											// y as� establecer si tiene permisos o no
    											'matchCallback' => function ($rule, $action) {
    											// Llamada al m�todo que comprueba si es un usuario simple
    											return User::isUserSimple ( Yii::$app->user->identity->id );
    											}
    											]
    											]
    											],
    											// Controla el modo en que se accede a las acciones, en este ejemplo a la acci�n logout
    					// s�lo se puede acceder a trav�s del m�todo post
    					'verbs' => [
    							'class' => VerbFilter::className (),
    							'actions' => [
    									'logout' => [
    											'post'
    									]
    							]
    					]
    					];
    }
    
    /**
     * Lists all Users models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new UsersSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Users model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Users model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Users();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            	
            	
            ]);
        }
    }

    /**
     * Updates an existing Users model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            	'id'=> $model->id,
            	'username' => $model->username,
            	'email' => $model->email,
            	'password' => $model->password,
            	'authKey'=> $model->authKey,
            	'accessToken' => $model->accessToken,	
            	'activate' => $model->activate,
            	'verification_code'=> $model->verification_code,	
            	'role' => $model->role,
            ]);
        }
    }
	
   
    
    /**
     * Deletes an existing Users model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Users model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Users the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Users::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
