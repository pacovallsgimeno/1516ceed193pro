<?php

namespace app\controllers;

use Yii;
use app\models\Conceptos;
use app\models\ConceptosSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\auth\QueryParamAuth;



/**
 * ConceptosController implements the CRUD actions for Conceptos model.
 */
class ConceptosController extends Controller
{
    
   
    
    /**
     * @inheritdoc
     */
    public function behaviors()
    {    
       	return [    
    				
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    
    /**
     * Lists all Conceptos models.
     * @return mixed
     */
    public function actionIndex()
    {
    	$userlog = Yii::$app->user->identity->id;
    	
    	    	
    	$searchModel = new ConceptosSearch(['userid'=> $userlog,]) ;
    	$dataProvider = $searchModel->search(Yii::$app->request->queryParams);
   
    	    
      
       	
        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,     		
        	
        ]);
    }

    /**
     * Displays a single Conceptos model.
     * @param integer $userid
     * @param integer $conceptoid
     * @return mixed
     */
    public function actionView($userid, $conceptoid)
    {
        return $this->render('view', [
            'model' => $this->findModel($userid, $conceptoid),
        ]);
    }

    /**
     * Creates a new Conceptos model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Conceptos();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'userid' => $model->userid, 'conceptoid' => $model->conceptoid]);
        } else {
            return $this->render('create', [
                'model' => $model,
            	'userid' => $model->userid,
            	'conceptoid' => $model->conceptoid
            		
            ]);
        }
    }

    /**
     * Updates an existing Conceptos model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $userid
     * @param integer $conceptoid
     * @return mixed
     */
    public function actionUpdate($userid, $conceptoid)
    {
    	$model = $this->findModel($userid, $conceptoid);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'userid' => $model->userid, 'conceptoid' => $model->conceptoid]);
        } else {
            return $this->render('update', [
                'model' => $model,
            	'userid' => $model->userid,
            	'conceptoid' => $model->conceptoid,
                'conceptodescrip' => $model->conceptodescrip,
            	
            ]);
        }
    }

    /**
     * Deletes an existing Conceptos model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $userid
     * @param integer $conceptoid
     * @return mixed
     */
    public function actionDelete($userid, $conceptoid)
    {
        $this->findModel($userid, $conceptoid)->delete();

        return $this->redirect(['index']);
    }
    

   /*  /**
     * Crear conceptos por defecto para nuevo usuarios.
     * 
   
     */
   /*  public function actionNewUser($userid)
    {
    	$contar = conceptos::find()->where('userid ='.$userid)->count();
    	
    	if ($contar == 0) {
    	 	for ($i=1; $i < 6; $i++){	
    			$model = new Conceptos();
    			$model-> userid = $userid;
    			$model-> conceptoid = $i;
    	 			switch ($i) {
  						case 1:
					         $model -> conceptodescrip = 'Ingresos ';
					         break;
					   case 2:
					         $model -> conceptodescrip = 'Gastos Vivienda';
					         break;
					   case 3:
					         $model -> conceptodescrip = 'Gastos Vehículo';
					         break;
					   case 4:
					       	$model -> conceptodescrip = 'Compras';
					       	break;
			          case 5:
			       		    $model -> conceptodescrip = 'Gastos Ocio';
			       		    break;
					}
				$model ->insert();
    		
    		}
    	} 
    }  */
    

    /**
     * Finds the Conceptos model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $userid
     * @param integer $conceptoid
     * @return Conceptos the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
   
    	
    
    protected function findModel($userid, $conceptoid)
    {
        if (($model = Conceptos::findOne(['userid' => $userid, 'conceptoid' => $conceptoid])) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
