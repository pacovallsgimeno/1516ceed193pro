-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 04-04-2016 a las 20:56:36
-- Versión del servidor: 10.1.9-MariaDB
-- Versión de PHP: 5.5.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `contafamily`
--
CREATE DATABASE IF NOT EXISTS `contafamily` DEFAULT CHARACTER SET utf8 COLLATE utf8_spanish_ci;
USE `contafamily`;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `conceptos`
--

DROP TABLE IF EXISTS `conceptos`;
CREATE TABLE `conceptos` (
  `userid` int(11) NOT NULL,
  `conceptoid` int(11) NOT NULL,
  `conceptodescrip` varchar(80) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `conceptos`
--

INSERT INTO `conceptos` (`userid`, `conceptoid`, `conceptodescrip`) VALUES
(1, 1, 'Ingreso Nóminas'),
(1, 2, 'Pago Hipoteca Casa'),
(1, 3, 'Compra Supermercado'),
(1, 4, 'Gasolina Coche Opel'),
(1, 5, 'Academias Niños'),
(2, 1, 'Ingresos '),
(2, 2, 'Gastos Vivienda'),
(2, 3, 'Gastos Vehículo'),
(2, 4, 'Compras'),
(2, 5, 'Gastos Ocio'),
(4, 1, 'Ingreso Nómina'),
(4, 2, 'Gastos de Casa'),
(4, 3, 'Gastos Coche'),
(4, 4, 'Gastos Compras'),
(4, 5, 'Gastos Hijos'),
(4, 6, 'Gastos Ocio'),
(6, 1, 'Ingresos '),
(6, 2, 'Gastos Vivienda'),
(6, 3, 'Gastos Vehículo'),
(6, 4, 'Compras'),
(6, 5, 'Gastos Ocio');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cuentas`
--

DROP TABLE IF EXISTS `cuentas`;
CREATE TABLE `cuentas` (
  `userid` int(11) NOT NULL,
  `cuentaid` int(11) NOT NULL,
  `cuentadescrip` varchar(80) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `cuentas`
--

INSERT INTO `cuentas` (`userid`, `cuentaid`, `cuentadescrip`) VALUES
(1, 1, 'Banco Santader'),
(1, 2, 'Bankia'),
(2, 1, 'Bankinter'),
(2, 2, 'La Caixa'),
(4, 1, 'Banco Santander'),
(4, 2, 'Bankia');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `diarios`
--

DROP TABLE IF EXISTS `diarios`;
CREATE TABLE `diarios` (
  `apunteid` int(11) NOT NULL,
  `userid` int(11) NOT NULL,
  `cuentaid` int(11) NOT NULL,
  `conceptoid` int(11) NOT NULL,
  `fecha` date NOT NULL,
  `Detalle` varchar(80) NOT NULL,
  `importe` float(10,2) NOT NULL,
  `punteo` tinyint(1) DEFAULT NULL,
  `observaciones` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `diarios`
--

INSERT INTO `diarios` (`apunteid`, `userid`, `cuentaid`, `conceptoid`, `fecha`, `Detalle`, `importe`, `punteo`, `observaciones`) VALUES
(1, 2, 1, 1, '2016-01-31', 'Nomina enero', 1350.00, 1, 'Sin Gastos'),
(2, 2, 1, 2, '2016-02-05', 'Alquiler enero', -300.00, 0, ''),
(3, 2, 2, 1, '2016-02-01', 'Traspaso de Bankinter', 200.00, 0, ''),
(4, 2, 1, 4, '2016-01-25', 'Compra Mercadona', -120.00, 0, ''),
(5, 2, 1, 2, '2016-02-01', 'Recibo de Luz', 150.00, 0, 'Pago 1/2016'),
(6, 2, 1, 1, '2016-01-27', 'Traspaso a Lacaixa', -300.00, 0, 'Traspaso Banco'),
(7, 2, 2, 1, '2016-01-27', 'Ingreso por Traspaso', 300.00, 0, 'Traspaso Bco '),
(8, 2, 1, 1, '2016-02-01', 'Traspaso de Bankinter', -200.00, 0, 'Traspaso'),
(9, 2, 1, 1, '2016-02-28', 'Nomina Febrero', 1350.00, 1, 'Sin Gastos'),
(10, 2, 1, 2, '2016-02-05', 'Alquiler febrero', -300.00, 0, ''),
(11, 2, 2, 1, '2016-02-01', 'Traspaso de Bankinter', 200.00, 0, ''),
(12, 2, 1, 4, '2016-02-25', 'Compra Mercadona', -155.00, 0, ''),
(13, 2, 1, 2, '2016-02-01', 'Recibo de Agua', 60.00, 0, 'Pago 1/2016'),
(14, 2, 1, 1, '2016-02-27', 'Traspaso a La caixa', -250.00, 0, 'Traspaso Banco'),
(15, 2, 2, 1, '2016-02-27', 'Ingreso por Traspaso', 250.00, 0, 'Traspaso Bco '),
(16, 2, 1, 3, '2016-02-01', 'Gasolina Coche', -100.00, 0, 'Consumo enero'),
(17, 2, 1, 3, '2016-03-10', 'Seguro Coche', -140.00, 0, 'Año 2016'),
(18, 2, 1, 5, '2016-03-20', 'Fin de Semana Calpe', -350.00, 0, 'Viaje Aniversario'),
(19, 2, 1, 4, '2016-02-29', 'Compras Supermercado', -380.00, 0, 'Total Febrero'),
(20, 2, 2, 5, '2016-02-28', 'Telefonos Moviles', -65.00, 0, 'Enero y Febrero'),
(21, 4, 1, 1, '2016-01-25', 'Nómina Enero', 1500.00, 0, ''),
(22, 4, 2, 1, '2016-01-27', 'Ingreso por Traspaso', 300.00, 0, 'Traspaso del Santander'),
(23, 4, 1, 1, '2016-01-27', 'Traspaso a Bankia', -300.00, 0, 'Traspaso Banco'),
(24, 4, 1, 2, '2016-02-05', 'Hipoteca Enero', -300.00, 0, 'Pago 1/2016');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `users`
--

DROP TABLE IF EXISTS `users`;
CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `username` varchar(50) NOT NULL,
  `email` varchar(80) NOT NULL,
  `password` varchar(250) NOT NULL,
  `authKey` varchar(250) NOT NULL,
  `accessToken` varchar(250) NOT NULL,
  `activate` tinyint(1) NOT NULL DEFAULT '0',
  `verification_code` varchar(250) NOT NULL,
  `role` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `users`
--

INSERT INTO `users` (`id`, `username`, `email`, `password`, `authKey`, `accessToken`, `activate`, `verification_code`, `role`) VALUES
(1, 'Administrador', 'info.contafamily@gmail.com', 'fsEit2InmVD..', '3a46dca84eb236f6f37e299b15576e336118b388a27dd46885337b7fe51576886ec0715fec2e57a00cdeb4289aa964b2515d24fb0da6a0ce5a5ec2c8251328c319c1ae8e0303efc29eb5b74d9674d2bac81b0df6ef5604aca8e28198eeaa62874d4a4334', '927fff6ca80442da1bb6d93ab8406e693a7ed0027d0dd8e12eb815d3f93c7073b863978e39ec45ef188047422f0d58541d2f34e0cba098c2e73e5f972cb5152a57c0a738434bd07c9cd34dc21f43b9c77757005691ccef1b284d01d3d8b642597db44f59', 1, 'c828bb3f', 2),
(2, 'demo', 'paco.valls.gimeno@gmail.com', 'fsyHMF9SqjlMc', '2fcdb9fb948be9441a6202ac404fbee0efcddcabd5b805bacddd17d864e7ceb18593c432fdccf706561e03d97e66a378115fa3aeeab5c028c545c5b15fca6e1573e06801cd848e301a417c7e25041f98bb6802398739122e45340a22d22496e1d9b68348', '16c58cf256fd7eab542bf320ddfaea3c9c36ec9c4dacc70bcd074745b7042607e78d122c1a21f7410bc96040770339c828bb3f92dd9b0d7c3f626f111cd9325e40ce21de66dce87e4ae054d8989510cb3db7f900d503ba552a441f760a27d51d1187b718', 1, '45e45803', 0),
(4, 'pacovalls', 'pacovalls@hotmail.es', 'fs9cGiDU5MyzI', '5bc54a97cb314fffdf88de19446793a0c5895b503ecad1f53cc7d15c9a5b18112533420f882a49d28fecbb66d72664685bc31f83a86f8894a9d77b66fad6a5956729c2febd8bbc3ca8b8113cf27dbda96a755a3fa99cb7bb9d7f9df8d1e9ba9232a7d966', '7b74a71f581cff89963c9e61e0bf2e6322317d59f98f37f130654717849d5945b0d51a7a80e8655fa183e6a2fe616a1e246fac70ee27698308785c134e1b51bd6dce055c213c43ad4442b75b85ab2e429d04531e5a37039c67f3f9889101d1ada32f88b7', 1, '', 1),
(6, 'paco', 'pacovalls67@gmail.com', 'fs9cGiDU5MyzI', '6c5d50976da4be2ba4683802ed569bf9d53993bc47cefb050fb06f92baebcb4f23fcbcb717be1db631ed8b086d46d16a4894cb98bd93064c58f07d54f696cc6c5212a04f394fe5b946ea557665bd9e4343766cec9be1a91b29b9038fbaba361109b11d6f', 'd3594444e26e987de5adc4b5b5d194c8f0a7c55372f91241f666373e38d701f7f4ea2b47f76b62ebed14819ea0b4441cdef4682243b2987bc999b1d3ff766d179ef48bd284efb4e17c04c60f131e7de834ca7369acf3a63d0545b2002d9d52afc08652d5', 1, '', 1);

-- --------------------------------------------------------

--
-- Estructura Stand-in para la vista `userstotalapuntes`
--
DROP VIEW IF EXISTS `userstotalapuntes`;
CREATE TABLE `userstotalapuntes` (
`userid` int(11)
,`Tapunte` bigint(21)
);

-- --------------------------------------------------------

--
-- Estructura Stand-in para la vista `userstotalcuentas`
--
DROP VIEW IF EXISTS `userstotalcuentas`;
CREATE TABLE `userstotalcuentas` (
`userid` int(11)
,`TCuenta` bigint(21)
);

-- --------------------------------------------------------

--
-- Estructura Stand-in para la vista `usertotalconcepto`
--
DROP VIEW IF EXISTS `usertotalconcepto`;
CREATE TABLE `usertotalconcepto` (
`userid` int(11)
,`TConcepto` bigint(21)
);

-- --------------------------------------------------------

--
-- Estructura para la vista `userstotalapuntes`
--
DROP TABLE IF EXISTS `userstotalapuntes`;

CREATE ALGORITHM=UNDEFINED DEFINER=`pacovalls`@`localhost` SQL SECURITY DEFINER VIEW `userstotalapuntes`  AS  select `diarios`.`userid` AS `userid`,count(`diarios`.`apunteid`) AS `Tapunte` from `diarios` group by `diarios`.`userid` ;

-- --------------------------------------------------------

--
-- Estructura para la vista `userstotalcuentas`
--
DROP TABLE IF EXISTS `userstotalcuentas`;

CREATE ALGORITHM=UNDEFINED DEFINER=`pacovalls`@`localhost` SQL SECURITY DEFINER VIEW `userstotalcuentas`  AS  select `cuentas`.`userid` AS `userid`,count(`cuentas`.`cuentaid`) AS `TCuenta` from `cuentas` group by `cuentas`.`userid` ;

-- --------------------------------------------------------

--
-- Estructura para la vista `usertotalconcepto`
--
DROP TABLE IF EXISTS `usertotalconcepto`;

CREATE ALGORITHM=UNDEFINED DEFINER=`pacovalls`@`localhost` SQL SECURITY DEFINER VIEW `usertotalconcepto`  AS  select `conceptos`.`userid` AS `userid`,count(`conceptos`.`conceptoid`) AS `TConcepto` from `conceptos` group by `conceptos`.`userid` ;

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `conceptos`
--
ALTER TABLE `conceptos`
  ADD PRIMARY KEY (`userid`,`conceptoid`),
  ADD KEY `conceptoid` (`conceptoid`);

--
-- Indices de la tabla `cuentas`
--
ALTER TABLE `cuentas`
  ADD PRIMARY KEY (`userid`,`cuentaid`),
  ADD KEY `cuentaid` (`cuentaid`);

--
-- Indices de la tabla `diarios`
--
ALTER TABLE `diarios`
  ADD PRIMARY KEY (`apunteid`),
  ADD KEY `cuentaid` (`cuentaid`,`userid`),
  ADD KEY `conceptoid` (`conceptoid`,`userid`);

--
-- Indices de la tabla `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `conceptos`
--
ALTER TABLE `conceptos`
  ADD CONSTRAINT `fk_concepto_user` FOREIGN KEY (`userid`) REFERENCES `users` (`id`);

--
-- Filtros para la tabla `cuentas`
--
ALTER TABLE `cuentas`
  ADD CONSTRAINT `fk_cuentas_user` FOREIGN KEY (`userid`) REFERENCES `users` (`id`);

--
-- Filtros para la tabla `diarios`
--
ALTER TABLE `diarios`
  ADD CONSTRAINT `fk_diario_concepto` FOREIGN KEY (`conceptoid`,`userid`) REFERENCES `conceptos` (`conceptoid`, `userid`),
  ADD CONSTRAINT `fk_diario_cuenta` FOREIGN KEY (`cuentaid`,`userid`) REFERENCES `cuentas` (`cuentaid`, `userid`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
