<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;


/* @var $this yii\web\View */
/* @var $model app\models\Conceptos */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="conceptos-form">

    <?php $form = ActiveForm::begin(); ?>
    
    <?= $form->field($model, 'userid')->hiddenInput()->label(false) ?>

	<?= $form->field($model, 'conceptoid')->hiddenInput()->label(false); ?>
    <?= $form->field($model, 'conceptodescrip')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Crear' : 'Actualizar', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>
    
    
    
   

</div>


