<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Conceptos */

$this->title = ' Nº de Concepto: ' .$model->conceptoid;
$this->params['breadcrumbs'][] = ['label' => 'Conceptos', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="conceptos-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Actualizar', ['update', 'userid' => $model->userid, 'conceptoid' => $model->conceptoid], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Borrar', ['delete', 'userid' => $model->userid, 'conceptoid' => $model->conceptoid], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => '¿Estás seguro de borrar este concepto?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
          //  'userid',
            'conceptoid',
            'conceptodescrip',
        ],
    ]) ?>

</div>
