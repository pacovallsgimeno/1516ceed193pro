<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Conceptos */

$this->title = 'Actualizar Conceptos: ' . ' ' . $model->conceptoid;
$this->params['breadcrumbs'][] = ['label' => 'Conceptos', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->userid, 'url' => ['view', 'userid' => $model->userid, 'conceptoid' => $model->conceptoid]];
$this->params['breadcrumbs'][] = 'Actualizar';
?>
<div class="conceptos-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    	'userid' => $model->userid,
    	'conceptoid' => $model->conceptoid,
    ]) ?>
    
   

</div>
