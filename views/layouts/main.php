<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;
use app\models\User;
use app\models\Users;
use yii\web\IdentityInterface;
use yii\helpers\Url;


AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>



<div class="wrap">
    <?php
   
    NavBar::begin([
        'brandLabel' => 'Contafamily',
        'brandUrl' => Yii::$app->homeUrl,
        'options' => [
            'class' => 'navbar-inverse navbar-fixed-top',
        ],
    ]);
    echo Nav::widget([
        'options' => ['class' => 'navbar-nav navbar-right'],
        'items' => [
        		       		      
           		['label' => 'Inicio', 'url' => ['/site/index']],  	
           		['label' => 'Sobre Contafamily', 'url' => ['/site/about'],'visible' =>Yii::$app->user->isGuest],
        		['label' => 'Registro', 'url' => ['/site/register'],'visible' =>Yii::$app->user->isGuest],
        	//	['label' => 'Contacto', 'url' => ['/site/contact']],
        		
        		Yii::$app->user->isGuest ?  (
        				''
        				)       				
        		 		 :
        				(     					
        					User::isUserSimple(Yii::$app->user->identity->id ) ? (
        							
        					['label' => 'Mi Contafamily', 'url' => ['site/user'],
        						'items' => [
        								['label' => 'Posición  Global', 'url' => ['site/user']],
        								['label' => 'Mis Apuntes', 'url' => ['diarios/index']],
        								['label' => 'Mis Cuentas', 'url' => ['cuentas/index']],
        								['label' => 'Mis Conceptos', 'url' => ['conceptos/index']],
        								]]
        					
        							
        					
        					
        						) :
        				
        							(	
        								User::isUserAdmin(Yii::$app->user->identity->id ) ?(
        								['label' => 'Menu Administrador', 'url' => ['site/admin'],
        								'items' => [
        									['label' => 'Estadisticas Usuarios', 'url' => ['site/admin']],
        									['label' => 'Gestion de Usuarios', 'url' => ['users/index']],
        								]] 
        								):(
        										['label' => 'Menu Demo', 'url' => ['site/demo'],
        										'items' => [
        												['label' => 'Posición Demo', 'url' => ['site/demo']],
        												['label' => 'Diario Demo', 'url' => ['diarios/index']],
        										]]
        										
        										
        										)	
        							  )
					
        			),
        		
        		Yii::$app->user->isGuest ?  (
        				['label' => 'Acceso Ususarios', 'url' => ['/site/login']]        	
	        		):(
        				['label' => 'Logout (' . Yii::$app->user->identity->username . ')', 'url' => ['site/logout'],
        				'linkOptions' => ['data-method' => 'post']]
        				),
        		['label' => 'Contacto', 'url' => ['/site/contact']],
        	//	['label' => 'Sobre Contafamily', 'url' => ['/site/about'],'visible' =>Yii::$app->user->isGuest],
        	 	['label' => 'Ayuda', 'url' => ['site/help'],
        						'items' => [
        								['label' => 'Ayuda Website', 'url' => ['site/help']],
        								['label' => 'Ayuda Demo', 'url' => ['site/helpdemo']],
        								['label' => 'Ayuda Usuarios', 'url' => ['site/helpuser']],
        								['label' => 'Ayuda Administrador', 'url' => ['site/helpadmin']],
        								]]
        		],
    		 
        		
        		
    	]);
        
    NavBar::end();
    ?>

    <div class="container">
        <?= Breadcrumbs::widget([
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        ]) ?>
        <?= $content ?>
    </div>
</div>

<footer class="footer">
    <div class="container">
        <p class="pull-left">Contafamily <?= date('Y') ?></p>

        <p class="pull-right"><?= Yii::powered() ?> <a href='https://es.linkedin.com/in/paco-valls-gimeno-7421296b'>- by Paco Valls</a></p>
    </div>
</footer>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
