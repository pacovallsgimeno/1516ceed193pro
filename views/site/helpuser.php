<?php

/* @var $this yii\web\View */

use yii\helpers\Html;




$this->title = 'Ayuda Usuarios ';
$this->params['breadcrumbs'][] = $this->title;
?>
<link rel="stylesheet" href="<?= Yii::getAlias('@web/css/help.css')?>">
<div class="site-help">
    
	<section class="jumbotron">
		<div class="container">
			<h1><?= Html::encode($this->title) ?></h1>
		</div>
	</section>

	<section class="main container">
		<div class="row">
			<section class="posts col-md-9">
				
				<article class="post clearfix">
					<a href="#" class="thumb pull-left">
						 <?= Html::img('@web/img/pguser.jpg', [
      						'alt' => 'inicio',
      						'class' => 'img-thumbnail',
						  ]) ?>
					</a>
					<h2 class="post-title">
						<p >Posición Global Usuario</p>
					</h2>
					
					<p class="post-contenido text-justify">
						Una vez que accedemos con nuestro usuario, la aplicación nos muestra de inicio la página con 
						nuestra posición global en la que se detalla el saldo de nuestras cuentas y la información de 
						los importes clasificados por nuestros conceptos. 
					</p>
					<p class="post-contenido text-justify">						  
						La primera vez que se acceda a la aplicación esta pagina de posición global sólo mostrará 
						la lista de conceptos que la aplicación establece por defecto para todos los usuarios, que más tarde 
						podremos modificar si lo deseamos. 
					</p>	
					<p class="post-contenido text-justify">
					Desde los diferentes menús de Mis Cuentas, Mis conceptos, podemos acceder a introducir nuestras 
					cuentas e introducir nuestros conceptos como puede verse en las siguientes pantallas, desde los 
					distintos botones o iconos que aparecen.
					</p>

					
				</article>

				<article class="post clearfix">
					<a href="#" class="thumb pull-left">
						 <?= Html::img('@web/img/cuentas.jpg', [
      						'alt' => 'Registro',
      						'class' => 'img-thumbnail',
						  ]) ?>
					</a>
					<h2 class="post-title">
						<p>Mantenimiento de  cuentas</p>
					</h2>
					<p class="post-contenido text-justify">
						Lo primero que deberíamos de hacer como usuarios registrados, es crear nuestras cuentas, ya
						que las necesitamos para clasificar y asignar nuestros apuntes del diarios. Desde esta pantalla
						podemos acceder a todas las opciones de creación de nuevas cuentas,	consulta, actualización y 
						borrado de cuentas. 

					</p>

					
				</article>

				<article class="post clearfix">
					<a href="#" class="thumb pull-left">
						< <?= Html::img('@web/img/conceptos.jpg', [
      						'alt' => 'Login',
      						'class' => 'img-thumbnail',
     					 ]) ?>
					</a>
					<h2 class="post-title">
						<p>Mantenimientos de  conceptos</p>
					</h2>
					<p class="post-contenido text-justify">
						La primera vez que se acceda a la aplicación mostrará los de conceptos establecidos 
						por defecto para todos los usuarios, que podemos modificar si lo deseamos.
						Desde esta vista podemos acceder a todas las opciones de creación de nuevos conceptos,
						consulta, actualización y borrado de  estos conceptos.

					</p>

					
				</article>
				
				<article class="post clearfix">
					<a href="#" class="thumb pull-left">
						< <?= Html::img('@web/img/diarios.jpg', [
      						'alt' => 'recover',
      						'class' => 'img-thumbnail',
     					 ]) ?>
					</a>
					<h2 class="post-title">
						<p>Diario de apuntes</p>
					</h2>
					<p class="post-contenido text-justify">
						Desde el diarios  se nos muestran los apuntes introducidos en nuestro diario  y 
						desde el gridview podemos acceder a la introducción de nuevos apuntes en el diario o  a consultar y modificar
						los apuntes existentes.
					</p>
				
				</article>
				
				<article class="post clearfix">
					<a href="#" class="thumb pull-left">
						< <?= Html::img('@web/img/crearapuntes.jpg', [
      						'alt' => 'recover',
      						'class' => 'img-thumbnail',
     					 ]) ?>
					</a>
					<h2 class="post-title">
						<p>Crear apuntes de diario</p>
					</h2>
					<p class="post-contenido text-justify">
						A partir de este momento ya estamos en disposición de introducir nuestros apuntes en el diarios. 
						Sólo tenemos que  seleccionar la cuenta y el concepto al que queremos asignar el apunte, así como la fecha, descripción e importe. 
						Podemos también completarlo con la opción de punteo e introducir comentarios en la observacione

					</p>

				
				</article>
				
				<article class="post clearfix">
					<a href="#" class="thumb pull-left">
						< <?= Html::img('@web/img/verapuntes.jpg', [
      						'alt' => 'recover',
      						'class' => 'img-thumbnail',
     					 ]) ?>
					</a>
					<h2 class="post-title">
						<p>Consultar registros</p>
					</h2>
					<p class="post-contenido text-justify">
						Desde la pantalla de consulta de los mantenimientos de  cuentas,
						conceptos y diarios, tenemos acceso a la actualización o borrado del registro selecionado.
					</p>
					
				
				</article>
				<article class="post clearfix">
					<a href="#" class="thumb pull-left">
						< <?= Html::img('@web/img/actualizar.jpg', [
      						'alt' => 'recover',
      						'class' => 'img-thumbnail',
     					 ]) ?>
					</a>
					<h2 class="post-title">
						<p>Actualizar registros</p>
					</h2>
					<p class="post-contenido text-justify">
						Una vez realizadas la modificación de registros o nuevas altas de registros estas 
						quedarán reflejadas tanto en la posición global del usuario y el resto datos de la aplicación. 
					</p>

				
				</article>
				
			</section>

			<aside class="col-md-3 hidden-xs hidden-sm">
				<h4 class="list-group-item-heading">Requiere login de usuario</h4>
					<p class="list-group-item-text">Debe registrase como usuario para que el acceso
													rápido funcione correctamente.</p><br>
													
				<h4>Acceso rápido Usuario</h4>
				
				<div class="list-group">
					
					<?= Html::a('Posición Global Usuario', ['/site/user'], ['class'=>'list-group-item']) ?>
					<?= Html::a('Mantenimiento de cuentas', ['/cuentas/index'], ['class'=>'list-group-item']) ?>
					<?= Html::a('Mantenimiento de conceptos', ['/conceptos/index'], ['class'=>'list-group-item']) ?>
					<?= Html::a('Diario de apuntes', ['/diarios/index'], ['class'=>'list-group-item']) ?>
					<?= Html::a('Crear Apuntes', ['/diarios/create'], ['class'=>'list-group-item']) ?>
					<?= Html::a('Consultar Registros', ['/diarios/view?id=21'], ['class'=>'list-group-item']) ?>
					<?= Html::a('Actualizar Registros', ['/diarios/update?id=21'], ['class'=>'list-group-item']) ?>
					
				</div>
				
				</aside>
				
				
			<aside class="col-md-3 hidden-xs hidden-sm">
				
			<h4>Acceso rápido ayudas</h4>
				
					
				
			
				
				<div class="list-group">
					<?= Html::a('Ayuda Website', ['/site/help'], ['class'=>'list-group-item']) ?>
					<?= Html::a('Ayuda Demo', ['/site/helpdemo'], ['class'=>'list-group-item']) ?>
					<?= Html::a('Ayuda Administrador', ['/site/helpadmin'], ['class'=>'list-group-item']) ?>
					
					
				</div>
				
				
			</aside>
		</div>
	</section>
	
    
</div>
