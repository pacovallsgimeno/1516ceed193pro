<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model app\models\ContactForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;

$this->title = 'Contacto';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-contact">
    <h1><?= Html::encode($this->title) ?></h1>

    <?php if (Yii::$app->session->hasFlash('contactFormSubmitted')): ?>

        <div class="alert alert-success">
        
        	Gracias por contactar con nosotros. Recibira nuestra respuesta lo antes posible.
         </div>

        <p>
             <?php if (Yii::$app->mailer->useFileTransport): ?>
                Debido a que la aplicación se encuentra en el modo de desarrollo, el correo electrónico no se envía, pero guarda como
                 un fichero <code><?= Yii::getAlias(Yii::$app->mailer->fileTransportPath) ?></code>.
                Por favor, configurar el <code> useFileTransport </ code>  y el <code> correo electrónico </ code>
                en el componente de aplicación que falta para permitir el envío de correo electrónico.
            <?php endif; ?>
        </p>

    <?php else: ?>
    
    	 <p>
           Si tiene preguntas sobre el fucionamiento u otra pregunta , por favor rellena el siguiente formulario para contactar con nosotros.
         </p>
         <p>
          Gracias.
         </p>

       

        <div class="row">
            <div class="col-lg-5">

                <?php $form = ActiveForm::begin(['id' => 'contact-form']); ?>

                    <?= $form->field($model, 'name')->textInput(['autofocus' => true]) ?>

                    <?= $form->field($model, 'email') ?>

                    <?= $form->field($model, 'subject') ?>

                    <?= $form->field($model, 'body')->textArea(['rows' => 6]) ?>

                    <?= $form->field($model, 'verifyCode')->widget(Captcha::className(), [
                        'template' => '<div class="row"><div class="col-lg-3">{image}</div><div class="col-lg-6">{input}</div></div>',
                    ]) ?>

                    <div class="form-group">
                        <?= Html::submitButton('Enviar', ['class' => 'btn btn-primary', 'name' => 'contact-button']) ?>
                    </div>

                <?php ActiveForm::end(); ?>

            </div>
        </div>

    <?php endif; ?>
</div>
