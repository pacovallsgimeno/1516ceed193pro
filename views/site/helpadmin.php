<?php

/* @var $this yii\web\View */

use yii\helpers\Html;




$this->title = 'Ayuda Administrador ';
$this->params['breadcrumbs'][] = $this->title;
?>
<link rel="stylesheet" href="<?= Yii::getAlias('@web/css/help.css')?>">
<div class="site-help">
    
	<section class="jumbotron">
		<div class="container">
			<h1><?= Html::encode($this->title) ?></h1>
		</div>
	</section>

	<section class="main container">
		<div class="row">
			<section class="posts col-md-9">
				
				<article class="post clearfix">
					<a href="#" class="thumb pull-left">
						 <?= Html::img('@web/img/adminestadistica.jpg', [
      						'alt' => 'inicio',
      						'class' => 'img-thumbnail',
						  ]) ?>
					</a>
					<h2 class="post-title">
						<p >Estadistica de Usuarios</p>
					</h2>
					
					<p class="post-contenido text-justify">
						Si accedemos como usuario administrador la aplicación nos muestra la página de información de usuarios, donde
						tenemos un ranking de usuarios por el uso que hacen de la aplicación, aparecen
						el número de registros introducidos en los diarios y el número de cuentas y conceptos de cada usuario.

					</p>

					
				</article>

				<article class="post clearfix">
					<a href="#" class="thumb pull-left">
						 <?= Html::img('@web/img/user.jpg', [
      						'alt' => 'Registro',
      						'class' => 'img-thumbnail',
						  ]) ?>
					</a>
					<h2 class="post-title">
						<p>Acceso a usuarios</p>
					</h2>
					<p class="post-contenido text-justify">
						Desde esta pantalla podemos acceder a la información de los usuarios, vemos su nombre, podemos enviarles
						un email haciendo click sobre su dirección  de email y tenemos acceso a la consuta y actualización de datos
						del usuario.

					</p>

					
				</article>

				<article class="post clearfix">
					<a href="#" class="thumb pull-left">
						< <?= Html::img('@web/img/viewuser.jpg', [
      						'alt' => 'Login',
      						'class' => 'img-thumbnail',
     					 ]) ?>
					</a>
					<h2 class="post-title">
						<p>Consulta de usuarios</p>
					</h2>
					<p class="post-contenido text-justify">
						En esta página de consulta tenemos vemos detallamente los datos de usuario seleccionado.
						Y podemos acceder a la opciones de mantenimiento. Únicamnte podemos proceder al borrado de un 
						usuario cuando este no tenga información asociada de cuentas, conceptos y apuntes en el diario.
					</p>

					
				</article>
				
				<article class="post clearfix">
					<a href="#" class="thumb pull-left">
						< <?= Html::img('@web/img/updateuser.jpg', [
      						'alt' => 'recover',
      						'class' => 'img-thumbnail',
     					 ]) ?>
					</a>
					<h2 class="post-title">
						<p>Activar usuarios</p>
					</h2>
					<p class="post-contenido text-justify">
						Desde la actulización de usuarios sólo es posible relizar la activación de desactivación del usuario, ya
						que la modificación de cualquier otro campo como el nombre, email o password dejaría al usuario sin acceso a
						a la aplicación.
					</p>
				
				</article>
				
				
				
			</section>

			<aside class="col-md-3 hidden-xs hidden-sm">
				<h4 class="list-group-item-heading">Requiere login Administrador</h4>
					<p class="list-group-item-text">Debe registrase como Administrador
													para que el acceso rápido 
													funcione correctamente.</p><br>
													
				<h4>Acceso rápido Administrador</h4>
				
				<div class="list-group">
					
					<?= Html::a('Estadistica de usuarios', ['/site/admin'], ['class'=>'list-group-item']) ?>
					<?= Html::a('Acceso a usuarios', ['/users/index'], ['class'=>'list-group-item']) ?>
					<?= Html::a('Consulta de usuarios', ['/users/view?id=4'], ['class'=>'list-group-item']) ?>
					<?= Html::a('Activar usuarios', ['/users/update?id=4'], ['class'=>'list-group-item']) ?>
				</div>
				
				</aside>
				
				
			<aside class="col-md-3 hidden-xs hidden-sm">
				
			<h4>Acceso rápido ayudas</h4>
				
					
				
			
				
				<div class="list-group">
					<?= Html::a('Ayuda Website', ['/site/help'], ['class'=>'list-group-item']) ?>
					<?= Html::a('Ayuda Demo', ['/site/helpdemo'], ['class'=>'list-group-item']) ?>
					<?= Html::a('Ayuda Usuario', ['/site/helpuser'], ['class'=>'list-group-item']) ?>
					
					
					
				</div>
				
				
			</aside>
		</div>
	</section>
	
    
</div>
