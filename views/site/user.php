<?php


$this->title = 'Posicion Global';
$this->params['breadcrumbs'][] = $this->title;
?>


   		

<H1> Posición Global </H1><br>
<h3>Saldo por Cuentas</h3><br>
		<table class="table table-hover">
		    <tr>
		        <th>Nº Cuentas</th>
		        <th>Nombre de la Cuenta</th>
		        <th>Saldo Cuenta</th>
		    </tr>
		    <?php foreach($model as $model): ?>
		    <tr>
		        <td><?= $model['num'] ?></td>
		        <td><?= $model['cuenta'] ?></td>
		        <td><?= $model['saldo'] ?></td>
		       
		    </tr>
		    <?php endforeach ?>
		</table><br>

		
<h3>Importes por Conceptos</h3><br>
		<table class="table table-hover">
		    <tr>
		        <th>Nº Concepto</th>
		        <th>Nombre del Concepto</th>
		        <th>Total Importe</th>
		    </tr>
		    <?php foreach($model1 as $model1): ?>
		    <tr>
		        <td><?= $model1['num'] ?></td>
		        <td><?= $model1['concepto'] ?></td>
		        <td><?= $model1['saldo'] ?></td>
		        		    </tr>
		    <?php endforeach ?>
		</table>