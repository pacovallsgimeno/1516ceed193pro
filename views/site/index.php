<?php

/* @var $this yii\web\View */
$this->title = 'Contafamily';

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
?>
<div class="site-index">


	<div class="jumbotron">
		<h1>Contafamily</h1>

		<h2>Toma el control de tu economía </h2>

		<!--       <p><a class="btn btn-lg btn-success" href="http://www.yiiframework.com">Acceso a Demo</a></p>-->
	</div>

	<div class="body-content">

		<div class="row">
			<div class="col-lg-4">
				<h2>Gestiona tus cuentas</h2>

				<p class="text-justify"> 
					Registramos mediante apuntes en un diario todos los movimientos de 
				    dinero que realizamos asignado su cuenta y concepto correspondiente para 
				    luego obtener la información detallada del uso de nuestro dinero, 
				    teniendo además información actualizada de los saldos disponibles de 
				    nuestras cuentas.
				</p>

				
			</div>
			<div class="col-lg-4">
				<h2>Información Segura</h2>

				<p class="text-justify">
				   La aplicación está diseñada de forma que cada usuarios sólo puede acceder 
				   a su información mediante su usuario y contraseña, utiliza además un sistema
				   de encriptación para el envío y de contraseñas y un sistema de seguridad para 
				   la recuperación password mediante un correo electrónico enviado al usuario
				   habilitando un sistema de seguridad que sólo permite el cambio de contraseña 
				   desde el dispositivo y navegador que ha solicitado el cambio.

				</p>

				
			</div>
			<div class="col-lg-4">
				<h2>Siempre disponible</h2>

				<p class="text-justify">
				Se puede acceder a la aplicación  desde cualquier dispositivo capaz de ejecutar un 
				navegador web, por tanto la aplicación permite el acceso desde dispositivos como Smartphone 
				o Tablet, que permiten al usuario registrar los apuntes en el momento de producirse,
				 así como el acceso a toda la información de saldos ,ingresos y gastos introducidas
				  en la aplicación.
				</p> 


				
			</div>
		</div>

	</div>
	
	<div class="jumbotron">
	
	
					  
		    	<p>
					<?= Html::a('Acceso a Demo', ['/site/logindemo'], ['class'=>'btn btn-lg btn-success']) ?>
				</p>
		
	</div>

</div>

