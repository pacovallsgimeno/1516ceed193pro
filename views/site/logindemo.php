<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model app\models\LoginForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Acceso Demo';
$this->params['breadcrumbs'][] = $this->title;

?>
<div class="site-login">
    <h1><?= Html::encode($this->title) ?></h1>

    <p>Acceso a demo para usuarios no registrados:</p>

    <?php $form = ActiveForm::begin([
        'id' => 'login-form',
        'options' => ['class' => 'form-horizontal'],
        'fieldConfig' => [
            'template' => "{label}\n<div class=\"col-lg-3\">{input}</div>\n<div class=\"col-lg-8\">{error}</div>",
            'labelOptions' => ['class' => 'col-lg-1 control-label'],
        ],
    ]); ?>

		
        <?= $form->field($model, 'username')->textInput(['readonly' => true, 'value' => 'demo']) ?>

        <?= $form->field($model, 'password')->passwordInput(['readonly' => true, 'value' => 'demouser']) ?>

       

        <div class="form-group">
            <div class="col-lg-offset-1 col-lg-11">
                <?= Html::submitButton('Login a Demo', ['class' => 'btn btn-primary', 'name' => 'login-button']) ?>
            </div>
        </div>
        

    <?php ActiveForm::end(); ?>
    
  		

    
</div>