<?php

/* @var $this yii\web\View */

use yii\helpers\Html;

$this->title = 'Sobre Contafamily ';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-about">
<link rel="stylesheet" href="<?= Yii::getAlias('@web/css/help.css')?>">
    
<div class="jumbotron">
		<h1>Sobre Contafamily</h1>

		

	</div>

	<div class="body-content">

		<div class="row">
			<div class="col-lg-12">
				
				<p class="text-justify">
				 Hoy en día, la utilización del dinero en efectivo es cada vez más escasa, 
				  la mayoría de las personas gestionamos nuestro dinero mediante las aplicaciones que nos 
				  facilitan las entidades bancarias y mediante tarjetas de crédito. Cobramos nuestras nóminas y  
				  pagamos nuestros recibos de hipotecas, alquiler, luz, agua, teléfono mediante nuestras cuentas bancarias y 
				  realizamos nuestras compras pagando en establecimientos con tarjetas bancarias, dejando la utilización del 
				  efectivo para contadas ocasiones..</p>
				  
				<p class="text-justify">
				 Es aquí donde surge la necesidad de crear Contafamily como una aplicación donde obtener toda nuestra
				  información financiera a modo de una contabilidad personal o familiar. La aplicación permite crear las distintas 
				  cuentas o agrupaciones (cuentas bancarias, tarjetas de crédito, cuenta de efectivo, etc) donde  gestionamos 
				  nuestro dinero y además crear una lista individualizada de los conceptos contables por los que queremos agrupar 
				  nuestros ingresos y gastos y obtener de esta forma la información financiera detallada de cómo utilizamos nuestro dinero.
				</p>
				</div>
				 
				
			
			
	</div>
	<div class="jumbotron">
				
		
				<h4>Licencia Creative Commons - Reconocimiento – NoComercial – CompartirIgual (by-nc-sa)</h4>
	</div>
		<div class="col-md-6 col-md-offset-3">
					 <?= Html::img('@web/img/creative_licencia.jpg', [
      						'class' => 'img-thumbnail',
						  ]) ?>
				</div>	
				
 </div>
	
	
    