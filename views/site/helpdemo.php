<?php

/* @var $this yii\web\View */

use yii\helpers\Html;




$this->title = 'Ayuda Usuario Demo ';
$this->params['breadcrumbs'][] = $this->title;
?>
<link rel="stylesheet" href="<?= Yii::getAlias('@web/css/help.css')?>">
<div class="site-help">
    
	<section class="jumbotron">
		<div class="container">
			<h1><?= Html::encode($this->title) ?></h1>
		</div>
	</section>

	<section class="main container">
		<div class="row">
			<section class="posts col-md-9">
				
				<article class="post clearfix">
					<a href="#" class="thumb pull-left">
						 <?= Html::img('@web/img/logindemo.jpg', [
      						'alt' => 'inicio',
      						'class' => 'img-thumbnail',
						  ]) ?>
					</a>
					<h2 class="post-title">
						<p >Acceso usuario demo</p>
					</h2>
					
					<p class="post-contenido text-justify">
						Desde el botón de acceso a demo de la página de inicio, la aplicación muestra la pantalla de login
						 para el usuario demo con su contraseña preestablecida que no permite ninguna modificación 
						 por parte del usuario.

					</p>

					
				</article>

				<article class="post clearfix">
					<a href="#" class="thumb pull-left">
						 <?= Html::img('@web/img/pgdemo.jpg', [
      						'alt' => 'Registro',
      						'class' => 'img-thumbnail',
						  ]) ?>
					</a>
					<h2 class="post-title">
						<p>Posición global Demo</p>
					</h2>
					<p class="post-contenido text-justify">
						 Que nos lleva a la página de Posición global del usuario demo y desde donde sólo 
						 tenemos acceso por parte del menú al diario demo.


					</p>

					
				</article>

				<article class="post clearfix">
					<a href="#" class="thumb pull-left">
						<?= Html::img('@web/img/diariodemo.jpg', [
      						'alt' => 'Login',
      						'class' => 'img-thumbnail',
     					 ]) ?>
					</a>
					<h2 class="post-title">
						<p>Diario de Apuntes</p>
					</h2>
					<p class="post-contenido text-justify">
						Desde el diarios demo se nos muestran los registros preestablecidos para el usuario demo y 
						desde el gridview podemos acceder a la introducción de nuevos apuntes en el diario o a modificar los 
						existentes para que el usuario compruebe el funcionamiento de la aplicación. </p>
					<p class="post-contenido text-justify">	
						Desde el gridview podemos realizar búsquedas y acceder a la actualización y borrado de registros.

					</p>

					
				</article>
				
				

				<article class="post clearfix">
					<a href="#" class="thumb pull-left">
						 <?= Html::img('@web/img/createdemo.jpg', [
      						'alt' => 'Login',
      						'class' => 'img-thumbnail',
     					 ]) ?>
					</a>
					<h2 class="post-title">
						<p>Crear apuntes de diario</p>
					</h2>
					<p class="post-contenido text-justify">
						Desde el formulario de creación de apuntes selecionamos la cuenta y el concepto deseados e introducimos los
						datos correspondientes a nuestro apunte del diarios. Desde ese momento queda incorporado al diarios y es contabiliado en 
						la posición global del usuario. </p>
					

					
				</article>
				
				<article class="post clearfix">
					<a href="#" class="thumb pull-left">
						 <?= Html::img('@web/img/viewdemo.png', [
      						'alt' => 'recover',
      						'class' => 'img-thumbnail',
     					 ]) ?>
					</a>
					<h2 class="post-title">
						<p>Ver y modificar apuntes</p>
					</h2>
					<p class="post-contenido text-justify">
						Una vez realizadas la modificación de registros o nuevas altas de apuntes estas 
						quedarán reflejadas en la posición global del usuario demo. 
					</p>
					<p class="post-contenido text-justify">	
						Una vez realizada la desconexión de la sesión demo cuando otro usuario vuelva a acceder los datos del 
						diario serán restablecidos de nuevo sin tener en cuenta las modificaciones de la 
						anterior sesión de demo.

					</p>
				
				</article>
				
			</section>

			<aside class="col-md-3 hidden-xs hidden-sm">
				<h4 class="list-group-item-heading">Requiere login Demo</h4>
					<p class="list-group-item-text">Debe registrase como usuario demo para que el acceso rápido 
													funcione correctamente.</p><br>
													
				<h4>Acceso rápido Demo</h4>
				
				<div class="list-group">
					
					<?= Html::a('Acceso a Demo', ['/site/logindemo'], ['class'=>'list-group-item']) ?>
					<?= Html::a('Posición Global', ['/site/demo'], ['class'=>'list-group-item']) ?>
					<?= Html::a('Diario de apuntes', ['/diarios/index'], ['class'=>'list-group-item']) ?>
					<?= Html::a('Crear apuntes de diario', ['/diarios/create'], ['class'=>'list-group-item']) ?>
					<?= Html::a('Ver y modificar apuntes', ['/diarios/view?id=1'], ['class'=>'list-group-item']) ?>
					
				</div>
				
				</aside>
				
				
			<aside class="col-md-3 hidden-xs hidden-sm">
				
			<h4>Acceso rápido ayudas</h4>
				
					
				
			
				
				<div class="list-group">
					<?= Html::a('Ayuda Website', ['/site/help'], ['class'=>'list-group-item']) ?>
					<?= Html::a('Ayuda Usuario', ['/site/helpuser'], ['class'=>'list-group-item']) ?>
					<?= Html::a('Ayuda Administrador', ['/site/helpadmin'], ['class'=>'list-group-item']) ?>
					
					
				</div>
				
				
			</aside>
		</div>
	</section>
	
    
</div>
