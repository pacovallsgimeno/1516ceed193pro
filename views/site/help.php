<?php

/* @var $this yii\web\View */

use yii\helpers\Html;




$this->title = 'Ayuda Contafamily ';
$this->params['breadcrumbs'][] = $this->title;
?>
<link rel="stylesheet" href="<?= Yii::getAlias('@web/css/help.css')?>">
<div class="site-help">
    
	<section class="jumbotron">
		<div class="container">
			<h1><?= Html::encode($this->title) ?></h1>
		</div>
	</section>

	<section class="main container">
		<div class="row">
			<section class="posts col-md-9">
				
				<article class="post clearfix">
					<a href="#" class="thumb pull-left">
						 <?= Html::img('@web/img/inicio.jpg', [
      						'alt' => 'inicio',
      						'class' => 'img-thumbnail',
						  ]) ?>
					</a>
					<h2 class="post-title">
						<p >Página de inicio Contafamily</p>
					</h2>
					
					<p class="post-contenido text-justify">
						Desde la página de inicio de la aplicación tenemos en la parte superior derecha el menú principal de la 
        				aplicación desde donde podemos acceder a las distintas opciones siendo las más importantes las de registro y 
        				acceso usuarios y contacto.
					</p>

					
				</article>

				<article class="post clearfix">
					<a href="#" class="thumb pull-left">
						 <?= Html::img('@web/img/registro.jpg', [
      						'alt' => 'Registro',
      						'class' => 'img-thumbnail',
						  ]) ?>
					</a>
					<h2 class="post-title">
						<p>Registrate en Contafamily</p>
					</h2>
					<p class="post-contenido text-justify">
						Desde el formulario de registro completamos nuestros datos de usuario , email y password y 
						la aplicación nos devuelve un email con un link por el cual validamos nuestro registro como usuarios 
						de la aplicación.

					</p>

					
				</article>

				<article class="post clearfix">
					<a href="#" class="thumb pull-left">
						 <?= Html::img('@web/img/login.jpg', [
      						'alt' => 'Login',
      						'class' => 'img-thumbnail',
     					 ]) ?>
					</a>
					<h2 class="post-title">
						<p>Acceso de usuarios de Contafamily</p>
					</h2>
					<p class="post-contenido text-justify">
						La página de acceso de usuario de la aplicación nos pide nuestro usuario y password para  
						acceder a la aplicación como usuarios registrados. También tenemos la opción de restablecer
						nuestro password mediante  un botón. la aplicación nos solicitará el email con que nos registramos 
						en la aplicación y no enviará un correo con un código de verificación y un link a la página donde 
						estableceremos nuestro  nuevo password.
					</p>

					
				</article>
				
				<article class="post clearfix">
					<a href="#" class="thumb pull-left">
						 <?= Html::img('@web/img/password.jpg', [
      						'alt' => 'recover',
      						'class' => 'img-thumbnail',
     					 ]) ?>
					</a>
					<h2 class="post-title">
						<p>Restablecer contraseña</p>
					</h2>
					<p class="post-contenido text-justify">
						la aplicación nos solicitará el email con que nos registramos 
						en la aplicación y no enviará un correo con un código de verificación y un link a la página donde 
						estableceremos nuestro  nuevo password.
					</p>
				
				</article>
				
				<article class="post clearfix">
					<a href="#" class="thumb pull-left">
						 <?= Html::img('@web/img/contacto.jpg', [
      						'alt' => 'recover',
      						'class' => 'img-thumbnail',
     					 ]) ?>
					</a>
					<h2 class="post-title">
						<p>Contacto</p>
					</h2>
					<p class="post-contenido text-justify">
						Por último la aplicación dispone de un formulario de contacto desde donde los usuarios 
						pueden enviar su comentarios o sugerencias al administrador de la aplicación .

					</p>

				
				</article>
				
			</section>

			<aside class="col-md-3 hidden-xs hidden-sm">
				<h4>Acceso rápido Invitados</h4>
				<div class="list-group">
					<?= Html::a('Inicio', ['/site/index'], ['class'=>'list-group-item']) ?>
					<?= Html::a('Registro', ['/site/register'], ['class'=>'list-group-item']) ?>
					<?= Html::a('Acceso de usuarios', ['/site/login'], ['class'=>'list-group-item']) ?>
					<?= Html::a('Restablecer contraseña', ['/site/recoverpass'], ['class'=>'list-group-item']) ?>
					<?= Html::a('Contacto', ['/site/contact'], ['class'=>'list-group-item']) ?>
					<?= Html::a('Acceso a Demo', ['/site/logindemo'], ['class'=>'list-group-item']) ?>
					<?= Html::a('Posición Global', ['/site/user'], ['class'=>'list-group-item']) ?>
					
				</div>
				
				
			</aside>
			
			<aside class="col-md-3 hidden-xs hidden-sm">
				
			<h4>Acceso rápido usuarios</h4>
				
					<h4 class="list-group-item-heading">Requiere logearse como usuario</h4>
					<p class="list-group-item-text">Debe registrase primero para que el acceso rápido de usuarios 
													funcione correctamente.</p>
				
			
				
				<div class="list-group">
					<?= Html::a('Ayuda Demo', ['/site/helpdemo'], ['class'=>'list-group-item']) ?>
					<?= Html::a('Ayuda Usuario', ['/site/helpuser'], ['class'=>'list-group-item']) ?>
					<?= Html::a('Ayuda Administrador', ['/site/helpadmin'], ['class'=>'list-group-item']) ?>
					
					
				</div>
				
				
			</aside>
		</div>
	</section>
	
    
</div>
