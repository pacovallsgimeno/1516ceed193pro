<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Cuentas */

$this->title = 'Actualizar Cuentas: ' . ' ' . $model->cuentaid;
$this->params['breadcrumbs'][] = ['label' => 'Cuentas', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->userid, 'url' => ['view', 'userid' => $model->userid, 'cuentaid' => $model->cuentaid]];
$this->params['breadcrumbs'][] = 'Actualizar';
?>
<div class="cuentas-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
