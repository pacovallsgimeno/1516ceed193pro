<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Cuentas */

$this->title = ' Nº de Cuenta: ' .$model->cuentaid;
$this->params['breadcrumbs'][] = ['label' => 'Cuentas', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="cuentas-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Actualizar', ['update', 'userid' => $model->userid, 'cuentaid' => $model->cuentaid], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Borrar', ['delete', 'userid' => $model->userid, 'cuentaid' => $model->cuentaid], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => '¿Estás seguro de borrar esta cuenta?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
           // 'userid',
            'cuentaid',
            'cuentadescrip',
        ],
    ]) ?>

</div>
