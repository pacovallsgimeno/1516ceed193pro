<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;
use app\models\Cuentas;
use app\models\Conceptos;
use yii\helpers\ArrayHelper;
use dosamigos\datepicker\DatePicker;



/* @var $this yii\web\View */
/* @var $model app\models\Diarios */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="diarios-form">

    <?php $form = ActiveForm::begin(); ?>
    
    <?php //  echo $form->field($model, 'apunteid')->textInput() ?>

    <?php $model->userid =$userlog // echo $form->field($model, 'userid')->textInput() ?>

    <?php  // $form->field($model, 'cuentaid')->textInput() ?>

    <?php // $form->field($model, 'conceptoid')->textInput() ?>
    
    <?= $form->field($model, 'cuentaid')->widget(Select2::classname(), [
    			'data' => ArrayHelper::map(Cuentas::find()->where(['userid'=> $userlog])->all(),'cuentaid', 'cuentadescrip'),
  				'options' => ['placeholder' => 'Selecciona una cuenta'],
    			/* 'pluginOptions' => [
       			 	'allowClear' => true
   				 ], */
		]); 
   	 ?>
   	 
   	 <?= $form->field($model, 'conceptoid')->widget(Select2::classname(), [
    			'data' => ArrayHelper::map(Conceptos::find()->where(['userid'=> $userlog])->all(),'conceptoid','conceptodescrip'),
  				'options' => ['placeholder' => 'Selecciona un concepto'],
    			/* 'pluginOptions' => [
       			 	'allowClear' => true
   				 ], */
		]); 
   	 ?>

    <?php // echo $form->field($model, 'fecha')->textInput() ?>
     <?= $form->field($model, 'fecha')->widget(
		    DatePicker::className(), [
		    	 'model' => $model,
		        // inline too, not bad
		        // 'inline' => false, 
		    	'language'=> 'es',	
		         // modify template for custom rendering
		       // 'template' => '<div class="well well-sm" style="background-color: #fff; width:250px">{input}</div>',
		        'clientOptions' => [
		            'autoclose' => true,
		            'format' => 'yyyy-mm-dd',
		        	'todayBtn' => true,
		         ],
			]);
    ?> 

    <?= $form->field($model, 'Detalle')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'importe')->textInput() ?>  
     		
   

    <?= $form->field($model, 'punteo')->checkbox()?>

    <?= $form->field($model, 'observaciones')->textArea(['rows'=>2,'cols'=>5]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Crear' : 'Actualizar', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
