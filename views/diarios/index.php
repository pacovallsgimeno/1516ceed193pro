<?php

use yii\helpers\Html;
use yii\grid\GridView;
use app\models\Diarios;
use kartik\select2\Select2;
use app\models\Cuentas;
use app\models\Conceptos;
use yii\helpers\ArrayHelper;
use dosamigos\datepicker\DatePicker;
use yii\grid\DataColumn;


/* @var $this yii\web\View */
/* @var $searchModel app\models\DiariosSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Diarios';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="diarios-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Crear Apuntes Diarios', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

           //'apunteid',
          // 'userid',
          // 'cuentaid',
        		// Llamo a la relacion getcuenta para mostrar el nombre de cuenta
           [
           		'attribute' => 'cuentaid',
           		'value'     => 'cuenta.cuentadescrip',
           		'format'    => 'raw',
				//'label'     => 'Cuenta',  
           		'filter'    => Select2::widget([
							    'model' => $searchModel,
	           				    'attribute' => 'cuentaid',
							    'data' => ArrayHelper::map(Cuentas::find()->where(['userid'=> $userlog])->all(),'cuentaid', 'cuentadescrip'),
							    'options' => [
							    		//'multiple' => true, 
							    		'placeholder' => 'Selecciona una Cuenta'
							    ],
           								'pluginOptions' => [
           										'allowClear' => true,
           								]
								]),
           						
        	],  
          //  'conceptoid',
         // Llamo a la relacion getconcepto para mostrar el nombre de cuenta
        		[
        		'attribute' => 'conceptoid',
        		'value'     => 'concepto.conceptodescrip',
        		'format'    => 'raw',
        		//'label'     => 'Concepto'
        		'filter'    => Select2::widget([
        					  'model' => $searchModel,
        					  'attribute' => 'conceptoid',
        					  'data' => ArrayHelper::map(Conceptos::find()->where(['userid'=> $userlog])->all(),'conceptoid','conceptodescrip'),
        					  'options' => [
        							//'multiple' => true,
        					 		 'placeholder' => 'Selecciona un Concepto'
        					 		 ],
        					 			  'pluginOptions' => [
        					   			       'allowClear' => true,
 							  			 ]
        				]),
		        			
		        				
        		],
           // 'fecha:date',
        		[
        			'attribute' =>'fecha',
        			'value'     => 'fecha',
        			'format'    => 'date',
        			'filter'	=>  DatePicker::widget([
						    'model' => $searchModel,
						    'attribute' => 'fecha',
        					'language'=> 'es',
						   // 'template' => '{addon}{input}',
						        'clientOptions' => [
						            'autoclose' => true,
						        	'format' => 'yyyy-mm-dd'
						        ],
        								
						]),
        				
   				 ],
            'Detalle',
            'importe',
            //'punteo',
        		[
        		'attribute' => 'punteo',
        		'format' => 'raw',
        		'value' => function ($model, $index, $widget) {
        		return Html::checkbox('diarios[]', $model->punteo, ['value' => $index, 'disabled' => true]);
        		},
        		],
        		
            'observaciones',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
