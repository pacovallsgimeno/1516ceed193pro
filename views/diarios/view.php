<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use app\models\Cuentas;
use app\models\Conceptos;
use yii\helpers\ArrayHelper;
use app\models\CuentasSearch;




/* @var $this yii\web\View */
/* @var $model app\models\Diarios */

$this->title = $model->apunteid;
$this->params['breadcrumbs'][] = ['label' => 'Diarios', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="diarios-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Actualizar', ['update', 'id' => $model->apunteid], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Borrar', ['delete', 'id' => $model->apunteid], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => '¿Estás seguro de borrar es apunte del diario?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
           //  'apunteid',
           //'userid',
         //  'cuentaid',
           //'conceptoid',
        	[
        			'label' => 'Nombre de la cuenta',
        			'attribute' => 'cuentaid',        		
        		    'value' => $model->cuenta->cuentadescrip,
 			   ],
        		
        	[
        		'label' => 'Concepto',
        		'attribute' => 'conceptoid',
        		'value' => $model->concepto->conceptodescrip,
        		
        	],
            'fecha:date',
            'Detalle',
            'importe',
            'punteo:boolean',        	
            'observaciones',
        ],
    ]) ?>

</div>
