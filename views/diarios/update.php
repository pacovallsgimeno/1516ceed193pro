<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Diarios */

$this->title = 'Update Diarios: ' . ' ' . $model->apunteid;
$this->params['breadcrumbs'][] = ['label' => 'Diarios', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->apunteid, 'url' => ['view', 'id' => $model->apunteid]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="diarios-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    	'userlog' => $userlog,	
    ]) ?>

</div>
