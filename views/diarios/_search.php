<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\DiariosSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="diarios-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'apunteid') ?>

    <?= $form->field($model, 'userid') ?>

    <?= $form->field($model, 'cuentaid') ?>

    <?= $form->field($model, 'conceptoid') ?>

    <?= $form->field($model, 'fecha') ?>

    <?= $form->field($model, 'Detalle') ?>

    <?= $form->field($model, 'importe') ?>

    <?= $form->field($model, 'punteo')?>

    <?= $form->field($model, 'observaciones') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
