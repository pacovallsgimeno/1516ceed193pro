<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Diarios */

$this->title = 'Create Diarios';
$this->params['breadcrumbs'][] = ['label' => 'Diarios', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="diarios-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    	'userlog' => $userlog,	
    ]) ?>

</div>
