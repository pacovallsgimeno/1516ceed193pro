<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\UsersSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Users';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="users-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?php // echo Html::a('Create Users', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'username',
            'email:email',
            'password',
            //'authKey',
            // 'accessToken',
            // 'activate:boolean',
        		[
        		'attribute' => 'activate',
        		'format' => 'raw',
        		'value' => function ($model, $index, $widget) {
        		return Html::checkbox('users[]', $model->activate, ['value' => $index, 'disabled' => true]);
        		},
        		],
            // 'verification_code',
             'role',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
