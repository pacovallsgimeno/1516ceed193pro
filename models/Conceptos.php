<?php

namespace app\models;

use Yii;
use yii\behaviors\BlameableBehavior;



/**
 * This is the model class for table "conceptos".
 *
 * @property integer $userid
 * @property integer $conceptoid
 * @property string $conceptodescrip
 *
 * @property Users $user
 * @property Diarios[] $diarios
 */
class Conceptos extends \yii\db\ActiveRecord
{
	    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'conceptos';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['conceptodescrip'], 'required'],
            [['userid', 'conceptoid'], 'integer'],
            [['conceptodescrip'], 'string', 'max' => 80],
            [['userid'], 'exist', 'skipOnError' => true, 'targetClass' => Users::className(), 'targetAttribute' => ['userid' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'userid' => 'Userid',
            'conceptoid' => 'Nº Concepto',
            'conceptodescrip' => 'Descripción del Concepto',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(Users::className(), ['id' => 'userid']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDiarios()
    {
        return $this->hasMany(Diarios::className(), ['conceptoid' => 'conceptoid', 'userid' => 'userid']);
    }
    
    public function behaviors()
    {
    	return [
    			'blameable' => [
    					'class' => BlameableBehavior::className(),
    					'createdByAttribute' => 'userid',
    					'updatedByAttribute' => 'userid',
    			],
    
    	];
    } 
    
    public function beforeSave ($insert)
    {
    	if (parent::beforeSave($insert)) {
    		If ($this->isNewRecord) {
    		$this->conceptoid = $this->getMaxId() ;
    		   return true;
    		}
    		else 
    		{
    			return true;
    		}
    	}
    }
    
    public function getMaxId()
    {
    	$conexion = Yii::$app->db;
    	$query = $conexion->createCommand('Select Max(conceptoid)from conceptos where userid='.$this->userid);
    	$max = $query->queryScalar();
    	
    	return $max+1;
    	
    	   
    }
    
    /**
     * Crear conceptos por defecto para nuevo usuarios.
     *
      
     */
    public function CreaConceptos($userid)
    {
    	$contar = conceptos::find()->where('userid ='.$userid)->count();
    	 
    	if ($contar == 0) {
    		for ($i=1; $i < 6; $i++){
    			$model = new Conceptos();
    			$model-> userid = $userid;
    			$model-> conceptoid = $i;
    			switch ($i) {
    				case 1:
    					$model -> conceptodescrip = 'Ingresos ';
    					break;
    				case 2:
    					$model -> conceptodescrip = 'Gastos Vivienda';
    					break;
    				case 3:
    					$model -> conceptodescrip = 'Gastos Vehículo';
    					break;
    				case 4:
    					$model -> conceptodescrip = 'Compras';
    					break;
    				case 5:
    					$model -> conceptodescrip = 'Gastos Ocio';
    					break;
    			}
    			$model ->insert();
    
    		}
    	}
    }
    
   
 
    }
