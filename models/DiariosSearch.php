<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Diarios;
use app\models\Cuentas;
use app\models\Conceptos;

/**
 * DiariosSearch represents the model behind the search form about `app\models\Diarios`.
 */
class DiariosSearch extends Diarios
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['apunteid', 'userid','cuentaid', 'conceptoid', 'punteo'], 'integer'],
            [['fecha', 'Detalle', 'observaciones'], 'safe'],
        	[['importe'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Diarios::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        	
        ]);
        
    	// $query->joinWith('cuenta');
       //  $query->joinWith('concepto');

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'apunteid' => $this->apunteid,
            'userid' => $this->userid,
            'cuentaid' => $this->cuentaid,
            'conceptoid' => $this->conceptoid,
            'fecha' => $this->fecha,
            'importe' => $this->importe,
            'punteo' => $this->punteo,
        ]);

        $query->andFilterWhere(['like', 'Detalle', $this->Detalle])
       		//  ->andFilterWhere(['like', 'cuentas.userid', $this->userid])
            //  ->andFilterWhere(['like', 'cuentas.cuentadescrip', $this->cuentaid])
            //  ->andFilterWhere(['like', 'conceptos.userid', $this->userid])
            //  ->andFilterWhere(['like', 'conceptos.conceptodescrip', $this->conceptoid])
              ->andFilterWhere(['like', 'observaciones', $this->observaciones]);

        return $dataProvider;
    }
}
