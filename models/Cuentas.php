<?php

namespace app\models;

use Yii;
use yii\behaviors\BlameableBehavior;

/**
 * This is the model class for table "cuentas".
 *
 * @property integer $userid
 * @property integer $cuentaid
 * @property string $cuentadescrip
 *
 * @property Users $user
 * @property Diarios[] $diarios
 */
class Cuentas extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'cuentas';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['cuentadescrip'], 'required'],
            [['userid', 'cuentaid'], 'integer'],
            [['cuentadescrip'], 'string', 'max' => 80],
            [['userid'], 'exist', 'skipOnError' => true, 'targetClass' => Users::className(), 'targetAttribute' => ['userid' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'userid' => 'Userid',
            'cuentaid' => 'Nº Cuenta',
            'cuentadescrip' => 'Nombre de la Cuenta',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(Users::className(), ['id' => 'userid']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDiarios()
    {
        return $this->hasMany(Diarios::className(), ['cuentaid' => 'cuentaid', 'userid' => 'userid']);
    }
    
    public function behaviors()
    {
    	return [
    			'blameable' => [
    					'class' => BlameableBehavior::className(),
    					'createdByAttribute' => 'userid',
    					'updatedByAttribute' => 'userid',
    			],
    
    	];
    }
    
    public function beforeSave ($insert)
    {
    	if (parent::beforeSave($insert)) {
    		If ($this->isNewRecord) {
    			$this->cuentaid = $this->getMaxId() ;
    			return true;
    		}
    		else
    		{
    			return true;
    		}
    	}
    }
    
    public function getMaxId()
    {
    	$conexion = Yii::$app->db;
    	$query = $conexion->createCommand('Select Max(cuentaid)from cuentas where userid='.$this->userid);
    	$max = $query->queryScalar();
    	 
    	return $max+1;
    	 
    
    }
}
