<?php
 
namespace app\models;
use Yii;
use yii\base\Model;
 
class FormRecoverPass extends model{
 
    public $email;
     
    public function rules()
    {
        return [
            ['email', 'required', 'message' => 'Campo requerido'],
            ['email', 'match', 'pattern' => "/^.{5,80}$/", 'message' => 'Mánimo 5 y míximo 80 caracteres'],
            ['email', 'email', 'message' => 'Formato no válido'],
        ];
    }
    
    public function attributeLabels()
    {
    	return [
    
    			'email' => 'Introduce tú email.',
    			    
    
    	];
}
}