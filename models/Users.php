<?php

namespace app\models;
use Yii;
use yii\db\ActiveRecord;
//use yii\behaviors\BlameableBehavior;

class Users extends ActiveRecord{
    
    public static function getDb()
    {
        return Yii::$app->db;
    }
    
    public static function tableName()
    {
        return 'users';
    }
    
    public function attributeLabels()
    {
    	return [
    			'id' => 'ID_Usuario',
    			'username' => 'Nombre de Usuario',
    			'email' => 'Email',
    			'password' => 'Password',
    			'authKey' => 'Auth Key',
    			'accessToken' => 'Access Token',
    			'activate' => 'Activado',
    			'verification_code' => 'Verification Code',
    			'role' => 'Rol',
    	];
    }
    public function rules()
    {
    	return [
    			[['username', 'email', 'password',], 'required'],
    			[['activate', 'role'], 'integer'],
    			[['username'], 'string', 'max' => 50],
    			[['email'], 'string', 'max' => 80],
    			[['password',], 'string', 'max' => 250],
    	];
    }
    
}
