<?php

namespace app\models;

use Yii;
use yii\behaviors\BlameableBehavior;

/**
 * This is the model class for table "diarios".
 *
 * @property integer $apunteid
 * @property integer $userid
 * @property integer $cuentaid
 * @property integer $conceptoid
 * @property string $fecha
 * @property string $Detalle
 * @property double $importe
 * @property integer $punteo
 * @property string $observaciones
 *
 * @property Conceptos $concepto
 * @property Cuentas $cuenta
 */
class Diarios extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'diarios';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [[ 'cuentaid', 'conceptoid','fecha', 'Detalle', 'importe'], 'required'],
            [['apunteid', 'userid', 'punteo'], 'integer'],
            [['fecha'], 'safe'],
            [['importe'], 'number'],
            [['Detalle'], 'string', 'max' => 80],
            [['observaciones'], 'string', 'max' => 255],
           // [['conceptoid', 'userid'], 'exist', 'skipOnError' => true, 'targetClass' => Conceptos::className(), 'targetAttribute' => ['conceptoid' => 'conceptoid', 'userid' => 'userid']],
           // [['cuentaid', 'userid'], 'exist', 'skipOnError' => true, 'targetClass' => Cuentas::className(), 'targetAttribute' => ['cuentaid' => 'cuentaid', 'userid' => 'userid']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'apunteid' => 'Apunteid',
            'userid' => 'Userid',
            'cuentaid' => 'Cuenta',
            'conceptoid' => 'Concepto',
            'fecha' => 'Fecha',
            'Detalle' => 'Descripcion del Apunte',
            'importe' => 'Importe',
            'punteo' => 'Punteo',
            'observaciones' => 'Observaciones',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getConcepto()
    {
        return $this->hasOne(Conceptos::className(), ['conceptoid' => 'conceptoid', 'userid' => 'userid']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCuenta()
    {
        return $this->hasOne(Cuentas::className(), ['cuentaid' => 'cuentaid', 'userid' => 'userid']);
    }
    
    public function behaviors()
    {
    	return [
    			'blameable' => [
    					'class' => BlameableBehavior::className(),
    					'createdByAttribute' => 'userid',
    					'updatedByAttribute' => 'userid',
    			],
    
    	];
    }
    public function beforeSave ($insert)
    {
    	if (parent::beforeSave($insert)) {
    		If ($this->isNewRecord) {
    			$this->apunteid = $this->getMaxApunteId() ;
    			return true;
    		}
    		else
    		{
    			return true;
    		}
    	}
    }
    
    public function getMaxApunteId()
    {
    	$conexion = Yii::$app->db;
    	$query = $conexion->createCommand('Select Max(apunteid)from diarios');
    	$max = $query->queryScalar();
    	 
    	return $max+1;
    }
    
   
    
    public function DatosDemo() {
    
    
    	// Borramos apuntes de usuario Demo
    	$conexion = Yii::$app->db;
    
    
    	$query = $conexion->createCommand('Delete from diarios where userid = 2')->execute();
    
    
    
    	// Insertamos juego de apuntes para nueva  Demo
    
    	$conexion1 = Yii::$app->db;
    
    	$sql = "INSERT INTO `diarios` (`apunteid`, `userid`, `cuentaid`, `conceptoid`, `fecha`, `Detalle`, `importe`, `punteo`, `observaciones`) VALUES
						(1, 2, 1, 1, '2016-01-31', 'Nomina enero', 1350.00, 1, 'Sin Gastos'),
						(2, 2, 1, 2, '2016-02-05', 'Alquiler enero', -300.00, 0, ''),
						(3, 2, 2, 1, '2016-02-01', 'Traspaso de Bankinter', 200.00, 0, ''),
						(4, 2, 1, 4, '2016-01-25', 'Compra Mercadona', -120.00, 0, ''),
						(5, 2, 1, 2, '2016-02-01', 'Recibo de Luz', 150.00, 0, 'Pago 1/2016'),
						(6, 2, 1, 1, '2016-01-27', 'Traspaso a Lacaixa', -300.00, 0, 'Traspaso Banco'),
						(7, 2, 2, 1, '2016-01-27', 'Ingreso por Traspaso', 300.00, 0, 'Traspaso Bco '),
						(8, 2, 1, 1, '2016-02-01', 'Traspaso de Bankinter', -200.00, 0, 'Traspaso'),
						(9, 2, 1, 1, '2016-02-28', 'Nomina Febrero', 1350.00, 1, 'Sin Gastos'),
						(10, 2, 1, 2, '2016-02-05', 'Alquiler febrero', -300.00, 0, ''),
						(11, 2, 2, 1, '2016-02-01', 'Traspaso de Bankinter', 200.00, 0, ''),
						(12, 2, 1, 4, '2016-02-25', 'Compra Mercadona', -155.00, 0, ''),
						(13, 2, 1, 2, '2016-02-01', 'Recibo de Agua', 60.00, 0, 'Pago 1/2016'),
						(14, 2, 1, 1, '2016-02-27', 'Traspaso a La caixa', -250.00, 0, 'Traspaso Banco'),
						(15, 2, 2, 1, '2016-02-27', 'Ingreso por Traspaso', 250.00, 0, 'Traspaso Bco '),
						(16, 2, 1, 3, '2016-02-01', 'Gasolina Coche', -100.00, 0, 'Consumo enero'),
						(17, 2, 1, 3, '2016-03-10', 'Seguro Coche', -140.00, 0, 'Año 2016'),
						(18, 2, 1, 5, '2016-03-20', 'Fin de Semana Calpe', -350.00, 0, 'Viaje Aniversario'),
						(19, 2, 1, 4, '2016-02-29', 'Compras Supermercado', -380.00, 0, 'Total Febrero'),
						(20, 2, 2, 5, '2016-02-28', 'Telefonos Moviles', -65.00, 0, 'Enero y Febrero');";
    
    	$query1 = $conexion1->createCommand($sql)->execute();
    
    }	
    
    public function Saldocuentas ($userlog) {
    	
    	
    	$conexion = Yii::$app->db;
    	 
    	
    	$query = $conexion->createCommand(
    			'select cuentas.cuentaid as num, cuentas.cuentadescrip as cuenta , Sum(diarios.importe) as saldo
    			from diarios
    			inner join cuentas ON diarios.cuentaid=cuentas.cuentaid and diarios.userid = cuentas.userid
    			where diarios.userid = :id
    			group by cuentas.cuentaid, cuentas.cuentadescrip
    			order by cuentas.cuentaid'
    			)
    			->bindValue(':id', $userlog)
    			
    	;
    			$model = $query->queryAll();
    			
    			return $model;
    			 
    }
    
    public function ImportesConcepto($userlog){
    	
    	$conexion1 = Yii::$app->db;
    	 
    	$query1 = $conexion1->createCommand(
    			'select conceptos.conceptoid as num, conceptos.conceptodescrip as concepto, Sum(diarios.importe) as saldo
    			from diarios
    			inner join conceptos ON diarios.conceptoid=conceptos.conceptoid and diarios.userid = conceptos.userid
    			where diarios.userid = :id
    			group by  conceptos.conceptoid, conceptos.conceptodescrip
    			order by conceptos.conceptoid'
    			)
    			->bindValue(':id', $userlog)
    			//->queryColumn();
    	;
    			$model1 = $query1->queryAll();
    			return $model1;
    }
}
